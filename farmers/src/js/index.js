import React from "react";
import { render } from "react-dom";
import { Provider } from "react-redux";
import store from "./store/index";
import App from "./components/App";
import { addFarmer } from "../js/actions/index";
import { selectFarmer } from "../js/actions/index";
import { searchFarmers } from "../js/actions/index";

window.store = store;
window.addFarmer = addFarmer;
window.selectFarmer = selectFarmer;
window.searchFarmers = searchFarmers;

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("app")
);


