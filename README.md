
## Job application portfolio for junior/entry level developer position



### LIST:

#### JAVA PROJECTS
**dungeon** - a game in Java. Moving a player through the printed dungeon with the aim to catch all
vampires before the given amount of turns is out;

**wormGame** - a classical snake game version in Java, with 3 different obsticles (snacks) in effect;

#### REACT NATIVE PROJECTS
**dreamCatcher** - a phone app to play relaxing soundtracks to help you fall asleep. WORK IN PROGRESS

**mobileBpm** - a DJ tool mobile app for playback and counting of BPM; WORK IN PROGRESS

#### REACT PROJECTS
**farmers** - React with Redux single-page web application. A simulation of a farmers database with a functionality of adding, viewing and searching the list. Runs on [Heroku](https://sheltered-peak-60232.herokuapp.com/).

**emissions** - project setup only. WORK IN PROGRESS